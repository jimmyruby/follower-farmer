is_running = false;
follow_rate = 60000  //time to wait in milliseconds

async function follow() {
    if (is_running) {
        return 
    }
    is_running = true;
    var followers = document.querySelectorAll('button');
    for (var i = 0; i < followers.length; i++) {
        if (followers[i].innerHTML.contains("Follow") && !followers[i].innerHTML.contains("Following")) {
            followers[i].click();
            await new Promise(r => setTimeout(r, follow_rate + Math.floor(Math.random() * 5000)));
        }
    }
    await new Promise(r => setTimeout(r, 100 + Math.floor(Math.random() * 20)));
    window.scrollBy(0, 2000);
    is_running = false;
}

setInterval(follow, 500)