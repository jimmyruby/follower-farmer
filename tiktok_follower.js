is_running = false;
follow_rate = 30000  //time to wait in milliseconds

async function follow() {
    if (is_running) {
        return 
    }
    is_running = true;
    var followers = document.querySelectorAll('button');
    for (var i = 0; i < followers.length; i++) {
        if (followers[i].innerHTML == "Follow") {
            followers[i].click();
            await new Promise(r => setTimeout(r, follow_rate + Math.floor(Math.random() * 5000)));
        }
    }
    await new Promise(r => setTimeout(r, 100 + Math.floor(Math.random() * 20)));
    var followers_div = document.querySelectorAll('.css-wq5jjc-DivUserListContainer')[0];
    followers_div.scrollBy(0, 2000);
    is_running = false;
}

setInterval(follow, 500)