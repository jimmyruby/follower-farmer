# Follower Farmer
Follower farmer consists of scripts to follow and unfollow accounts on instagram and tiktok

## How to Use

#### Instagram Follower
1. Navigate to an instagram page who's followers you want to follow
2. Open 'inspect element' (cmd + option + i) on Chrome
3. Press (cmd + shift + M) to put the webpage into mobile mode
4. Click on the account's "Followers" button
5. Click the 'Console' tab in the inspect element window
6. Paste the contents of the `insta_follower.js` file in the 'Console' tab and press enter
7. Keep the browser tab open in the foreground. 
    - You can turn the screen brightness off or switch tabs, but you'll occasionally have to re-open the page to let the bot scroll to new followers

#### TikTok Follower
1. Navigate to an tiktok page who's followers you want to follow
2. Open 'inspect element' (cmd + option + i) on Chrome
3. Press (cmd + shift + M) to put the webpage into mobile mode
4. Click on the account's "Followers" button
5. Click the 'Console' tab in the inspect element window
6. Paste the contents of the `tiktok_follower.js` file in the 'Console' tab and press enter
7. Keep the browser tab open in the foreground. 
    - You can turn the screen brightness off or switch tabs, but you'll occasionally have to re-open the page to let the bot scroll to new followers

#### Instagram Unfollower
1. Navigate to your instagram profile
2. Open 'inspect element' (cmd + option + i) on Chrome
3. Press (cmd + shift + M) to put the webpage into mobile mode
4. Click on your 'Following' button
5. Click the 'Console' tab in the inspect element window
6. Paste the contents of the `insta_unfollower.js` file in the 'Console' tab and press enter
7. Keep the browser tab open in the foreground. 
    - You can turn the screen brightness off or switch tabs, but you'll occasionally have to re-open the page to let the bot scroll to new followers

## "You can't follow accounts right now"
At some point Instagram will stop you from following accounts. I'm still trying to figure out a way around this. For now, you just have to wait a few hours to a few days and restart the process
